
require 'spec_helper'

describe Zrduino::Tool do

  describe "v 1.6.1" do 

    let(:zrdu) { fixture_installation( "v161" ) }

    it 'sees the right tools for arduino/avr' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      expect(arduino_avr.tools.map { |tool| tool.name } ).to eq %I{avrdude}
    end

    it 'sees the right path and config for arduino/avr avrdude' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      avrdude = arduino_avr.tools_by_name[:avrdude]
      expect(avrdude.config_path).to eq "{runtime.ide.path}/hardware/tools/avr/etc/avrdude.conf"
      expect(avrdude.cmd_path).to eq "{runtime.ide.path}/hardware/tools/avr/bin/avrdude"
    end

    it 'sees the right recipes for arduino/avr avrdude' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      avrdude = arduino_avr.tools_by_name[:avrdude]
      expect(avrdude.recipes.map { |recipe| recipe.name } ).to eq %I{upload program erase bootloader}
    end

    it 'sees the right recipe parameters and paths for avr avrdude program' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      program_recipe = arduino_avr.tools_by_name[:avrdude].recipes_by_name[:program]
      expect(program_recipe).to_not be_nil
      expect(program_recipe.params).to eq ({ verbose: "-v", quiet: "-q -q" })
    end

    it 'sets the right map' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      avrdude = arduino_avr.tools_by_name[:avrdude]
      expect(avrdude.build_settings).to eq ({ 
          'cmd.path' => avrdude.cmd_path,
          'config.path' => avrdude.config_path
      })
    end

  end

end

