
require 'spec_helper'

describe Zrduino::Platform do

  describe "v 1.6.1" do 

    let(:zrdu) { fixture_installation( "v161" ) }

    it 'creates the right build settings' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      expect(arduino_avr.build_settings).to eq ({
          "build.arch" => "avr", 
          "name" => "Arduino AVR Boards", 
          "version" => "1.6.1", 
          "compiler.path" => "{runtime.ide.path}/hardware/tools/avr/bin/", 
          "compiler.c.cmd" => "avr-gcc", 
          "compiler.c.flags" => "-c -g -Os -w -ffunction-sections -fdata-sections -MMD", 
          "compiler.c.elf.flags" => "-w -Os -Wl,--gc-sections", 
          "compiler.c.elf.cmd" => "avr-gcc", 
          "compiler.c.elf.extra_flags" => "", 
          "compiler.c.extra_flags" => "", 
          "compiler.S.flags" => "-c -g -x assembler-with-cpp", 
          "compiler.S.extra_flags" => "", 
          "compiler.cpp.cmd" => "avr-g++", 
          "compiler.cpp.flags" => "-c -g -Os -w -fno-exceptions -ffunction-sections -fdata-sections -fno-threadsafe-statics -MMD", 
          "compiler.cpp.extra_flags" => "", 
          "compiler.ar.cmd" => "avr-ar", 
          "compiler.ar.flags" => "rcs", 
          "compiler.ar.extra_flags" => "", 
          "compiler.objcopy.cmd" => "avr-objcopy", 
          "compiler.objcopy.eep.flags" => "-O ihex -j .eeprom --set-section-flags=.eeprom=alloc,load --no-change-warnings --change-section-lma .eeprom=0", 
          "compiler.objcopy.eep.extra_flags" => "", 
          "compiler.elf2hex.flags" => "-O ihex -R .eeprom", 
          "compiler.elf2hex.cmd" => "avr-objcopy", 
          "compiler.elf2hex.extra_flags" => "", 
          "compiler.ldflags" => "", 
          "compiler.size.cmd" => "avr-size", 
          "build.extra_flags" => "", 
          "build.usb_manufacturer" => "\"Unknown\"", 
          "build.usb_flags" => "-DUSB_VID={build.vid} -DUSB_PID={build.pid} '-DUSB_MANUFACTURER={build.usb_manufacturer}' '-DUSB_PRODUCT={build.usb_product}'"
      })
    end

    it 'sees the right recipes' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      expect( arduino_avr.recipes.map { |recipe| recipe.name } ).to eq %I{c.o c.combine cpp.o S.o ar objcopy.eep objcopy.hex size}
    end

    it 'sees the build settings for its recipes' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      size_recipe = arduino_avr.recipes_by_name[:size]
      expect( size_recipe.build_settings ).to eq ({
          :regex => "^(?:\\.text|\\.data|\\.bootloader)\\s+([0-9]+).*", 
          :"regex.data" => "^(?:\\.data|\\.bss|\\.noinit)\\s+([0-9]+).*", 
          :"regex.eeprom" => "^(?:\\.eeprom)\\s+([0-9]+).*"
      })
    end

  end

end

