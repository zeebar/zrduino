
require 'spec_helper'

describe Zrduino::Arduino do

  describe "v 1.6.1" do 

    let(:zrdu) { fixture_installation( "v161" ) }

    it 'consults the config to find the ide' do
      expect(zrdu.config['arduino.ide']).to eq (fixture_root_path + "arduino" + "v161").to_s
    end

    it 'sees the installed version number' do
      expect(zrdu.arduino.version).to eq  "1.6.1"
    end

    it 'can parse boards.txt' do
      board_config = Zrduino::installation.arduino.parse_hw_config( fixture_root_path + "arduino" + "v161" + "Contents" + "Java" + "hardware" + "arduino" + "avr" + "boards.txt" )
      expect(board_config.children.size).to be > 0
    end

    it 'can parse platform.txt' do
      platform_config = Zrduino::installation.arduino.parse_hw_config( fixture_root_path + "arduino" + "v161" + "Contents" + "Java" + "hardware" + "arduino" + "avr" + "platform.txt" )
      expect(platform_config.children.size).to be > 0
    end

    it 'knows its supported architectures' do
      expect( zrdu.arduino.architectures.map { |arch| arch.name } ).to eq %I{avr sam}
    end

    it 'parses board definitions' do
      expect( zrdu.arduino.avr.boards.map { |board| board.name } ).to eq %I{yun uno diecimila nano mega megaADK leonardo micro esplora mini ethernet fio bt LilyPadUSB lilypad pro atmegang robotControl robotMotor teensy31 teensy30 teensyLC teensy2 teensypp2}
      expect( zrdu.arduino.sam.boards.map { |board| board.name } ).to eq %I{arduino_due_x_dbg arduino_due_x}
    end

    it 'sees the right architectures' do
      expect(zrdu.arduino.architectures.map { |a| a.name } ).to eq [ :avr, :sam ]
    end

    it 'sees the right platforms' do
      expect(zrdu.arduino.architectures[0].platforms.map { |p| p.vendor_name }.sort ).to eq [ :arduino, :teensy ]
    end

    it 'sees the right cores' do
      expect( zrdu.arduino.boards_by_name[:nano].core.name ).to eq :arduino
      expect( zrdu.arduino.boards_by_name[:mega].core.name ).to eq :arduino
      expect( zrdu.arduino.boards_by_name[:teensy31].core.name ).to eq :teensy3
    end

    it 'sees the right variants' do
      expect( zrdu.arduino.boards_by_name[:nano].variant.name ).to eq :eightanaloginputs
      expect( zrdu.arduino.boards_by_name[:mega].variant.name ).to eq :mega
    end

    it 'builds global settings' do
      ide_path = fixture_root_path + "arduino" + "v161"
      expect( zrdu.arduino.build_settings ).to eq ({
        'runtime.hardware.path' => (ide_path + "Contents" + "Java" + "hardware").to_s,
        'runtime.ide.path' => ide_path.to_s,
        'runtime.ide.version' => "1.6.1",
        'runtime.os' => "macosx"
      })
    end

  end

end

