# See: http://code.google.com/p/arduino/wiki/Platforms

bob.type=nice

bob.hex=0x8041
bob.number=123
bob.number.a=1
bob.number.b=2
bob.some.deeper.number=3
bob.string="Some String Yun"
bob.stringref={bob.string}
bob.stringref={bob.string} {bob.name}
bob.number.b.beos=3

############# some comment #################################################

bob.name=Some Other Tea

bob.long=16000000L

