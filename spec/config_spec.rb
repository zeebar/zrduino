require 'spec_helper'

describe Zrduino do

  describe Zrduino::Config do
 
    before :each do 
      @zce = ENV['ZRDUINO_CONFIG'] 
      @home = ENV['HOME'] 
    end

    after :each do 
      ENV['ZRDUINO_CONFIG'] = @zce
      ENV['HOME'] = @home
    end

    it 'uses config overide path if provided' do
      ENV['ZRDUINO_CONFIG'] = nil
      expect(Zrduino::Config.config_path(Pathname.new("/tmp/x.config"))).to eq Pathname('/tmp/x.config')
    end

    it 'uses envar unless config overide path is provided' do
      ENV['ZRDUINO_CONFIG'] = "/tmp/y.config"
      expect(Zrduino::Config.config_path).to eq Pathname('/tmp/y.config')
    end

    it 'uses envar unless config overide path is provided' do
      ENV['ZRDUINO_CONFIG'] = nil
      ENV['HOME'] = '/tmp'
      expect(Zrduino::Config.config_path).to eq Pathname('/tmp/.zrduino')
    end

  end

end

