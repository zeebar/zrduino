
require 'spec_helper'

describe Zrduino::Library do

  describe "v 1.6.1" do 

    let(:zrdu) { fixture_installation( "v161" ) }

    it 'sees the right libraries at the aruino level' do
      expect(zrdu.arduino.libraries.map { |library| library.name } ).to eq %I{WiFi}
    end

    it 'global lib has the right platform and architecture' do
      expect(zrdu.arduino.libraries_by_name[:WiFi].platform).to be_nil
      expect(zrdu.arduino.libraries_by_name[:WiFi].architectures).to eq zrdu.arduino.architectures
    end

    it 'sees the right libraries for arduino/avr platform' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      expect(arduino_avr.libraries.map { |library| library.name } ).to eq %I{SPI}
    end

    it 'arduino/avr lib has the platform' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      expect(arduino_avr.libraries_by_name[:SPI].architectures).to eq [arduino_avr.architecture]
    end

    it 'sees the right path and config for arduino/avr SPI' do
      arduino_avr = zrdu.arduino.avr.platforms_by_vendor_name[ :arduino ]
      spi = arduino_avr.libraries_by_name[:SPI]
      expect(spi.version).to eq "1.0"
      expect(spi.path).to eq fixture_root_path + "arduino" + "v161" + "Contents" + "Java" + "hardware" + "arduino" + "avr" + "libraries" + "SPI"
      expect(spi.src_path).to eq fixture_root_path + "arduino" + "v161" + "Contents" + "Java" + "hardware" + "arduino" + "avr" + "libraries" + "SPI" + "src"
    end

  end

end

