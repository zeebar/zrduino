$LOAD_PATH.unshift File.expand_path('../../lib', __FILE__)

require 'zrduino'

def tmp_dir
  @tmp_dir ||= begin
    td = Pathname.new(File.expand_path('../../tmp', __FILE__))
    td.mkdir unless td.exist?
    td
  end
end

def fixture_sketch_path( name )
  fixture_root_path + "sketches" + name
end

def fixture_installation( version = "v161" )
  Zrduino::installation( config_file: create_zrduino_config( version, { 'arduino' => { 'ide' => (fixture_root_path + "arduino" + version).to_s } } ) )
end

def create_zrduino_config( config_name, config_hash )
  config_path = tmp_dir + config_name
  File.open(config_path, "w") { |zrduino_config_yml| zrduino_config_yml << config_hash.to_yaml }
  config_path
end

def non_existent_file
  file_name = File.join(File.dirname(__FILE__), "xxx", "does_not_exist")
  raise "should not exist: #{file_name}" if File.exist?(file_name)
  file_name
end

def fixture_root_path
  @fixture_root_path ||= Pathname.new(File.dirname(__FILE__)) + "fixtures"
end
