
describe Zrduino::HwConf do

  it 'parses simple spec maps' do 
    spec_map = Zrduino::HwConf.new( fixture_root_path + "arduino" + "basic.txt" )
    expect(spec_map.root.children.map { |child| child.name }).to eq %w{bob}
    expect(spec_map.root.children_by_name['bob'].children.map { |child| child.name }).to eq %w{type hex number some string stringref name long}
  end

  it 'parses simple spec maps' do 
    spec_map = Zrduino::HwConf.new( fixture_root_path + "arduino" + "basic.txt" )
    expect( spec_map['bob.number.a'] ).to eq "1"
    expect( spec_map['bob.number.b'] ).to eq "2"
  end

  it 'parses simple spec maps' do 
    spec_map = Zrduino::HwConf.new( fixture_root_path + "arduino" + "basic.txt", "beos" )
    expect( spec_map['bob.number.b'] ).to eq "3"
  end

end

