
require 'spec_helper'

describe Zrduino::Board do

  describe "v 1.6.1" do 

    let(:zrdu) { fixture_installation( "v161" ) }

    it 'can answer simple queries about a simple sketch' do

      motor_knob_path = fixture_sketch_path( 'motor_knob' )

      sketch = zrdu.sketch( motor_knob_path ) do |s|
        s.source_file 'stepper_test.cpp'
        s.include_library :stepper
      end

      expect( sketch.name ).to eq :motor_knob
      expect( sketch.path ).to eq motor_knob_path
      expect( sketch.libraries.map { |library| library.name } ).to eq %I{stepper}

    end

  end

end

