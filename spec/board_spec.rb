
require 'spec_helper'

describe Zrduino::Board do

  describe "v 1.6.1" do 

    let(:zrdu) { fixture_installation( "v161" ) }

    it 'creates the right build settings' do
      zrdu = Zrduino::installation( config_path: fixture_root_path + "arduino" + "v161" )
      apro = zrdu.arduino.boards_by_name[:pro]
      expect(apro).to_not be_nil
      expect(apro.core.name).to eq :arduino
      expect(apro.variant.name).to eq :eightanaloginputs
      expect(apro.build_settings).to eq ({
          "build.board" => "AVR_PRO",
          "build.core" => "arduino",
          "build.core.path" => apro.core.path.to_s,
          "build.variant" => "eightanaloginputs",
          "build.variant.path" => apro.variant.path.to_s
      })
    end

    it 'creates the right tool settings' do
      zrdu = Zrduino::installation( config_path: fixture_root_path + "arduino" + "v161" )
      apro = zrdu.arduino.boards_by_name[:pro]
      expect(apro.tool_settings(:upload)).to eq ({
          "upload.tool" => "avrdude", 
          "upload.protocol" => "arduino"
      })

    end

    it 'sees board menus' do
      zrdu = Zrduino::installation( config_path: fixture_root_path + "arduino" + "v161" )
      diecimila = zrdu.arduino.boards_by_name[:diecimila]
      expect(diecimila.menus_by_name.keys).to eq %I{cpu}
      diecimila_cpu = diecimila.menus_by_name[:cpu]
      expect(diecimila_cpu.choices.map { |choice| choice.name }).to eq %I{atmega328 atmega168}
      diecimila_cpu_atmega168 = diecimila_cpu.choices_by_name[:atmega168]
      expect(diecimila_cpu_atmega168).to_not be_nil
      expect(diecimila_cpu_atmega168.title).to eq "ATmega168"
      expect(diecimila_cpu_atmega168.build_settings).to eq ({
          "upload.maximum_size"=>"14336", 
          "upload.maximum_data_size"=>"1024", 
          "upload.speed"=>"19200", 
          "bootloader.high_fuses"=>"0xdd", 
          "bootloader.extended_fuses"=>"0x00", 
          "bootloader.file"=>"atmega/ATmegaBOOT_168_diecimila.hex", 
          "build.mcu"=>"atmega168"
      })
    end

  end

end

