require 'spec_helper'

describe Zrduino do

  it 'has a version number' do
    expect(Zrduino::VERSION).not_to be nil
  end

  it 'loads some installation even without extra help' do
    expect(Zrduino::installation).not_to be nil
  end

end

