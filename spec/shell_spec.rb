
require 'spec_helper'

describe Zrduino::Shell do

  describe "v 1.6.1" do 

    let(:zrdu) { fixture_installation( "v161" ) }

    it 'creates can be created' do
      sh = Zrduino::Shell.new( {}, Pathname.new("/tmp") )
    end

    it 'can do something' do
      sh = Zrduino::Shell.new( {}, Pathname.new("/tmp") )
      stdout_lines = []
      stderr_lines = []
      rc = sh.execute( "echo hello world | uniq -c" ) do |action, atts|
        if action == :stdout
          stdout_lines << atts[:line]
        elsif action == :stderr
          stdout_lines << atts[:line]
        end
      end
      expect( stdout_lines ).to eq ["1 hello world"]
      expect( stderr_lines ).to eq []
      expect( rc ).to eq 0
    end

    it 'can do more than 1 thing' do
      sh = Zrduino::Shell.new( {}, Pathname.new("/tmp") )
      expect( sh.execute( "echo hello world | grep hello" ) ).to eq 0
      expect( sh.execute( "echo hello world | grep goodbye" ) ).to_not eq 0
    end

    it 'can set envars' do
      sh = Zrduino::Shell.new( {"SOME_A" => "1", "SOME_B" => "2"}, Pathname.new("/tmp") )
      expect( sh.execute( "echo hello $SOME_A | grep 'hello 1'" ) ).to eq 0
      expect( sh.execute( "echo hello $SOME_B | grep \"hello 2\"" ) ).to eq 0
      expect( sh.execute( "echo hello $SOME_C | grep 'hello 3'" ) ).to_not eq 0
    end

  end

end


