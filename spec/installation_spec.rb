require 'spec_helper'

describe Zrduino do

  describe Zrduino::Installation do
 
    describe 'default config' do

      before :each do
        @zce = ENV['ZRDUINO_CONFIG']
        @home = ENV['HOME']
      end

      after :each do
        ENV['ZRDUINO_CONFIG'] = @zce
        ENV['HOME'] = @home
      end

      it 'uses envar unless config overide path is provided' do
        ENV['ZRDUINO_CONFIG'] = non_existent_file
        ENV['HOME'] = non_existent_file
        ENV['ARDUINO_IDE'] = non_existent_file
        zrdu = Zrduino.installation
        expect(zrdu).to_not be_nil
      end

    end

  end

end

