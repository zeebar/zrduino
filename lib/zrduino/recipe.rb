
module Zrduino

  class Recipe

    attr_reader :context, :name, :pattern, :params

    def initialize( context, name, recipe_config )
      @context = context
      @name = name

      @params = {}
      if params_node = recipe_config.children_by_name['params']
        params_node.children_by_name.each do |name, param_config_child|
          @params[name.to_sym] = param_config_child.value
        end
      end
      
      @recipe_config = recipe_config
      @pattern = @recipe_config['pattern']
    end

    def build_settings 
      @build_settings ||= begin
        rbs = {}
        @recipe_config.each_descendent do |descendent|
          if descendent.name != 'pattern' && descendent.value
            bs_path = descendent.path( @recipe_config )
            rbs[bs_path.to_sym] = descendent.value
          end
        end
        rbs
      end
    end

  end

end

