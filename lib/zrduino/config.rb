
require 'yaml'
require 'pathname'

module Zrduino

  class Config

    def Config.default_config_path
      Pathname.new( ENV['HOME'] ) + ".zrduino"
    end
    
    def Config.config_path( config_path_override = nil )
       if config_path_override 
         config_path_override 
       elsif config_envar = ENV['ZRDUINO_CONFIG']
         Pathname.new( config_envar )
       else
         Config.default_config_path
       end
     end

     def Config.load_config( path = nil )
       if path
         raise "no such zrduino config file: #{path}" unless path.exist?
         raise "not a zrduino config file: #{path}" if path.directory?
         YAML::load( path.read ) || {}
       else
         {}
       end
     end

     def Config.save_config( config_hash, path = nil )
       new_config_path = path || Config.default_config_path
       tmp_config_path = Pathname.new(new_config_path.to_s + ".tmp")
       new_config.open do |config_file|
         config_file << config_hash.to_yaml
       end
       tmp_config_path.rename_to( new_config_path )
     end


     attr_reader :config_hash

     def initialize( config_hash )
       @config_hash = config_hash
     end

     def merge!( other )
       @config_hash.merge!(other)
     end

     def []=( name, value )
       r = @config_hash
       nms = name.split(".")
       nms[0..-2].each do |nm|
         r = (r[nm] ||= {})
       end
       r[nms.last] = value
     end

     def []( name, default = nil )
       r = @config_hash
       nms = name.split(".")
       nms[0..-2].each do |nm|
         r = (r[nm] ||= {})
       end
       r[nms.last] ||= default
     end

     def to_yaml
       @config_hash.to_yaml
     end

  end

end

