
module Zrduino 

  module RecipeContainer

    def recipes_by_name
      @recipes_by_name ||= begin
        rbn = {}
        rrn = recipes_config_root
        rrn.each_descendent do |descendent|
          if descendent.name == 'pattern' && descendent.value
            recipe_node = descendent.parent
            recipe_name_path = []
            node = recipe_node
            while node != rrn
              recipe_name_path.insert(0, node.name)
              node = node.parent
            end
            recipe_name = recipe_name_path.join(".").to_sym
            rbn[recipe_name.to_sym] = Recipe.new( self, recipe_name, recipe_node )
          end
        end
        rbn
      end
    end

    def recipes
      recipes_by_name.values
    end

  end

  class Platform

    attr_reader :vendor_name, :architecture, :hw_config, :path, :title, :version
    

    def initialize( architecture, vendor_name, path = nil )
      @vendor_name = vendor_name
      @architecture = architecture
      @path = path || (@architecture.arduino.hardware_path + @vendor_name + architecture.name.to_s)
      @platform_config_path = @path + "platform.txt"
      unless @platform_config_path.exist?
        raise "missing platform configuration " + @platform_config_path.to_s
      end
      @hw_config = @architecture.arduino.parse_hw_config( @platform_config_path )
      @title = @hw_config['name']
      @version = @hw_config['version']
    end


    def boards_by_name
      @boards_by_name ||= begin
        bbn = {}
        board_path = @path + "boards.txt"
        if board_path.exist?
          board_config = @architecture.arduino.parse_hw_config( board_path )
          board_config.children.each do |child|
            unless child.name == 'menu'
              bbn[child.name] = Board.new( self, child.name.to_sym, child )
            end
          end
        end
        bbn
      end
    end

    def boards
      boards_by_name.values
    end


    def cores_path 
      @cores_path ||= @path + 'cores'
    end

    def cores_by_name
      @cores_by_name ||= begin
        cbn = {}
        cores_path.children.each do |core_path|
          if core_path.directory? 
            core = Core.new( self, core_path )
            cbn[core.name] = core
          end
        end
        cbn
      end
    end

    def cores
      cores_by_name.values
    end


    def variants_path 
      @variants_path ||= @path + 'variants'
    end

    def variants_by_name
      @variants_by_name ||= begin
        vbn = {}
        variants_path.children.each do |variant_path|
          if variant_path.directory? 
            variant = Variant.new( self, variant_path )
            vbn[variant.name] = variant
          end
        end
        vbn
      end
    end

    def variants
      variants_by_name.values
    end


    def tools_by_name 
      @tools_by_name ||= begin
        tbn = {}
        @hw_config.root.children_by_name['tools'].children_by_name.each do |tool_name, tool_config|
          tbn[tool_name.to_sym] = Tool.new( self, tool_name.to_sym, tool_config )
        end
        tbn
      end
    end

    def tools
      tools_by_name.values
    end


    def build_settings
      bs = { 'build.arch' => architecture.name.to_s }
      @hw_config.root.children_by_name.each do |child_name, child|
        unless %w{recipe tools}.include? child_name 
          child.each_descendent do |descendent|
            if value = descendent.value
               bs[descendent.path(@hw_config.root)] = value
            end
          end
        end
      end
      bs
    end


    def libraries_path 
      @libraries_path ||= @path + 'libraries'
    end

    include LibrariesContainer

    def lc_arduino
      @architecture.arduino
    end

    def lc_platform
      self
    end

    include RecipeContainer

    def recipes_config_root
      @hw_config.root.children_by_name['recipe']
    end

  end

end
