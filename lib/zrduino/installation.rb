require 'logger'

module Zrduino

  class Installation

     attr_reader :config, :arduino_root, :arduino

     def initialize( atts = nil )
       atts ||= {}
       path = Config.config_path(atts[:config_file])
       @logger = atts[:logged] || Logger.new(STDOUT)
       @config = if path && path.exist? 
         Config.new(Config.load_config(path))
       else
         Config.new({})
       end
       @config.merge!(atts)
       default_arduino_ide_path = Arduino.default_ide_path || (raise "dont know how to guess default arduino path")
       @arduino_ide_path = Pathname.new(@config['arduino.ide', ENV['ARDUINO_IDE'] || default_arduino_ide_path.to_s]) || (raise "no arduino.path")
       raise "cannot find aruino ide at " + @arduino_ide_path.to_s unless @arduino_ide_path
     end

     def arduino
       @arduino ||= Arduino.new( @arduino_ide_path )
     end

     def sketch( path, atts = {} )
       new_sketch = Sketch.new( self, path, atts )
       yield(new_sketch) if block_given?
       new_sketch
     end

     def hardware_root 
       @hardware_root ||= arduino_ide_path + "Contents" + "Java" + "hardware"
     end

     def tools_root 
       @tools_root ||= hardware_ide_path + "tools"
     end

     def boards_by_name
       @boards_by_name ||= begin
         {}
       rescue Exception => e
         raise "could not load board configuration: #{e.message}"
       end
     end

  end

end

