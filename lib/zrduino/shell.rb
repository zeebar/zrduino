
require 'open3'
require 'shellwords'

module Zrduino

  class Shell

    SHELL = "/bin/sh"
    DONE_PATTERN = "[-[zdru done]-]:"
    CHANNEL_IDS = [:stdin, :stdout, :stderr]
    OUTPUT_CHANNEL_IDS = CHANNEL_IDS[1..2]

    def initialize( env, work_dir )
      @pid = nil
      @env = env
      @work_dir = work_dir
    end

    def start_shell
      unless @pid
        @stdin, @stdout, @stderr, @wait_thr = Open3.popen3( @env, SHELL, chdir: @work_dir.to_s, rlimit_core: 0, in: "/dev/null" )
        @pid = @wait_thr.pid
      end
    end

    def execute( external_command )
      start_shell unless @wait_thr && @wait_thr.alive?
      buffers = ["", "", ""]
      output_channels = [@stdout, @stderr]
      done_status = nil
      Thread.new do |t|
        @stdin << external_command + "\n" 
        @stdin << "/bin/echo #{Shellwords.escape(DONE_PATTERN)} $?\n"
        @stdin.flush
      end
      while @wait_thr.alive? && done_status.nil?
        pending_channels = IO.select(output_channels, [], [], 0.25)
        if pending_channels && pending_channels != []
          pending_channels[0].each do |pending_channel|
            pcx = if pending_channel == @stdout
              0 
            elsif pending_channel == @stderr
              1 
            else 
              yield( :bad_channel, channel: pending_channel ) if block_given?
            end
            begin
              s = pending_channel.readpartial(1024)
#             yield( OUTPUT_CHANNEL_IDS[pcx], data: s ) if block_given?
              buffers[pcx] << s
            rescue EOFError
              yield( :eof, channel: OUTPUT_CHANNEL_IDS[pcx] ) if block_given?
            end
            while eolx = (buffers[pcx].index( "\r" ) || buffers[pcx].index( "\n" ))
              line = buffers[pcx][0...eolx].strip
              buffers[pcx] = buffers[pcx][eolx+1 .. -1]
              unless line.empty?
                if line.index( DONE_PATTERN ) == 0 
                  done_status = line[DONE_PATTERN.size..-1].to_i
#                 yield( :status, status: done_status ) if block_given?
                else
                  yield( OUTPUT_CHANNEL_IDS[pcx], line: line ) if block_given?
                end
              end
            end
          end
        end
      end
      done_status
    end

  end

end

