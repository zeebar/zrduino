
module Zrduino

  class Variant

    attr_reader :platform, :path, :name

    def initialize( platform, path )
      @platform = platform
      @path = path
      @name = path.basename.to_s.to_sym
    end

  end

end

