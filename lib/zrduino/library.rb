
module Zrduino 

  class Library

    attr_reader :name, :arduino, :platform, :path, :properties
    
    def initialize( arduino, platform, config_path )
      @config_path = config_path
      @path = @config_path.parent
      @arduino = arduino
      @platform = platform
      @properties = {}
      File.open( @config_path ) do |config|
        while line = config.gets
          unless line.index('#') == 0 || line.strip == ""
            if ex = line.index("=")
              property_name = line[0..(ex-1)]
              property_value = line[(ex+1)..-1].strip
              @properties[property_name.to_sym] = property_value
            end
          end
        end
      end
      @name = @properties[:name].to_sym || @path.basename.to_s.to_sym
    end


    def version
      @version ||= @properties[:version]
    end


    def architectures
      @architectures ||= begin
        if architectures_s = @properties[:architectures]
          architectures_s.strip!
          if architectures_s == "*"
            @arduino.architectures
          else
            architectures_s.split.map { |architecture_name| @arduino.architectures_by_name[architecture_name.to_sym] }
          end
        else
          @arduino.architectures
        end
      end
    end


    def src_path
      @src_path ||= begin
        src = @path + 'src'
        if src.exist?
          src
        else
          @path
        end
      end
    end

  end

end

