
module Zrduino 

  class Sketch

    attr_reader :name, :path, :installation, :libraries
    
    def initialize( installation, path, atts = nil  )
      @installation = installation
      @path = path
      @name = atts[:name] || @path.basename.to_s.to_sym
      @libraries = []
      @source_files = []
    end

    def shell
      @shell = Shell.new( {}, target_dir )
    end

    def source_file( source_file )
      @source_files << source_file
    end

    def include_library( library )
      found_library = if library.is_a?(Symbol)
        @installation.arduino.libraries_by_name[library] || (raise "no such library: #{library}, try #{@installation.arduino.libraries_by_name.keys}")
      else
        library
      end
      @libraries << found_library
      found_library
    end

    def compile( logger, board, build_settings, source_file, include_search_path, target_dir ) 
      o_file = nil
      compile_settings = {}
      compile_settings.merge(build_settings)
      compile_settings['includes'] = include_search_path.map { |p| "-I#{p.to_s}" }.join(" ")
      compile_settings['source_file'] = source_file.to_s
      extname = source_file.extname
      language = case extname.to_s
      when 'c'
        :"c.o"
      when 'cpp'
        :"cxx.o"
      when 'S'
        :"S.o"
      else 
        raise "unrecognized source file extensio: #{extname} for #{source_file}"
      end
      recipe = board.recipe( language )
      raise "no #{language} recipe for board #{board.name}" unless recipe
      basename = source_file.basename(extname)
      o_file = target_dir + (basename + ".o")
      compile_settings['object_file'] = o_file.to_s
      shell_cmd = recipe.apply( buld_settings )
      cmd_status = shell.execute( shell_cmd ) do |action, atts|
        yield( action, atts )
      end
      unless cmd_status 
        raise "could not compile #{source_file.to_s}"
      end
      o_file
    end

    def archive( logger, board, build_settings, o_files, target_dir ) 
      a_file = nil
      context = {}
      context.merge(build_settings)
      # TODO: write
      a_file
    end

    def link( logger, board, build_settings, main_o_file, a_file, target_dir ) 
      hex_file = nil
      context = {}
      context.merge(build_settings)
      context['object_files'] = [main_o_file].map do |o_file|
        o_file.to_s
      end.join(' ')
      board.recipe(:"c.combine")
      context['archive_file'] = a_file.to_s
      # TODO: write
      hex_file
    end

    def build( board, opts = {} )

      target_dir = opts[:target_dir] || (@path + 'build' + board.name)

      yield( :phase, phase: :prep, target_dir: target_dir, board: board ) if block_given?
      unless target_dir.exist?
        yield( :phase, phase: :mkdir, target_dir: target_dir ) if block_given?
        target_dir.mkpath
      end

      logger = Logger.new( target_dir + "build.log" )

      @shell = nil

      begin
        include_search_path = [@path]
        libraries.each { |library| include_search_path << library.include_path }
        include_search_path << board.variant.include_path
        include_search_path << board.core.include_path
        build_settings = {}
        build_settings.merge( installation.arduino.build_settings )
        build_settings.merge( board.build_settings )
        build_settings['build.path'] = target_dir.to_s
        build_settings['build.system.path'] = include_search_path.map { |p| p.to_s }.join(" ")
        build_settings['build.project_name'] = @name
        o_files = []
        yield( :phase, phase: :compile ) if block_given?
        source_files.each do |source_file|
          yield( :compile, language: language, source: source_file ) if block_given?
          o_file = compile( logger, board, build_settings, source_file, include_search_path, target_dir ) do |event, atts| 
            yield( event, atts ) if block_given?
          end
          o_files << o_file
        end
        yield( :phase, phase: :ar ) if block_given?
        ar = archive( logger, board, build_settings, o_files, target_dir ) do |event, atts|
          yield( event, atts ) if block_given?
        end
        yield( :phase, phase: :link ) if block_given?
        hex = link( logger, board, build_settings, ar, target_dir ) do |event, atts|
          yield( event, atts ) if block_given?
        end
        yield( :phase, phase: :done, hex: hex ) if block_given?
        hex
      ensure 
        if @shell
          @shell.shutdown 
          @shell = nil
        end
        nil
      end
      
    end

  end

end
