
module Zrduino

  class HwConfNode

    attr_reader :name, :children_by_name, :value, :parent

    def initialize(os, name = nil, parent = nil)
      @os = os
      @name = name
      @children_by_name = {}
      @parent = parent
    end

    def [](name)
      get(name)
    end

    def children
      @children_by_name.values
    end

    def each_descendent
      yield(self) if block_given?
      @children_by_name.each do |child_name, child|
        child.each_descendent { |descendent| yield(descendent) }
      end
    end

    def get(name)
      if dot_pos = name.index(".")
        prefix = name[0..(dot_pos - 1)]
        tail = name[(dot_pos + 1)..- 1]
        if child = @children_by_name[prefix]
          child.get(tail)
        else
          nil
        end
      else
        if child = @children_by_name[name]
          child.value
        else
          nil
        end
      end
    end

    def set(name, value)
      if name.nil?
        @value = value
      else
        suffix = nil
        prefix = if dot_pos = name.index(".")
          suffix = name[(dot_pos + 1)..-1]
          name[0..dot_pos - 1]
        else
          name
        end
        if suffix.nil? && name && name == @os
          @value = value
        else
          child = (@children_by_name[prefix] ||= HwConfNode.new(@os, prefix, self))
          child.set(suffix, value)
        end
      end
    end

    def path( top = nil )
      if @parent && @parent != top
        @parent.path(top) + "." + name
      else
        name || '#'
      end
    end

  end

end

