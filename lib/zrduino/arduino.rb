
require 'logger'

module Zrduino

  module LibrariesContainer

    def libraries_by_name
      @libraries_by_name ||= begin
        lbn = {}
        libraries_path.children.each do |library_path|
          if library_path.directory?
            properties_file = library_path + "library.properties"
            if properties_file.exist?
              library = Library.new( lc_arduino, lc_platform, properties_file )
              lbn[library.name] = library
            end
          end
        end
        lbn
      end
    end

    def libraries
      libraries_by_name.values
    end

  end


  class Arduino

    class << self 

      def default_ide_path
        Pathname.new("/Applications/Arduino.app")
      end

    end

    attr_reader :root_path


    def initialize( root_path )
      @root_path = root_path
      @os_platform = "macosx"
    end

    def parse_hw_config( path )
      raise "could not parse #{path.to_s} - no such file" unless path.exist?
      begin
        HwConf.new( path )
      rescue Exception => e
        raise "could not parse #{path.to_s}: #{e.message}"
      end
    end

    def acj_path
      @acj_path ||= @root_path + "Contents" + "Java"
    end

    def hardware_path
      @hardware_path ||= acj_path + "hardware"
    end

    def libraries_path
      @libraries_path ||= acj_path + "libraries"
    end

    def tools_path
      @tools_path ||= acj_path + "tools"
    end

    def version_path
      @version_path ||= acj_path + "lib" + "version.txt"
    end

    def version
      @version ||= version_path.read
    end

    def hardware_vendor_paths
      hardware_path.children.select { |child| child.basename.to_s != "tools" }
    end

    def architectures_by_name
      @architectures_by_name ||= Hash[%I{avr sam}.map do |arch_name|
        [arch_name, Architecture.new( self, arch_name )]
      end]
    end

    def sam
      architectures_by_name[:sam]
    end
    
    def avr
      architectures_by_name[:avr]
    end
    
    def architectures
      @architectures = architectures_by_name.values
    end

    def boards_by_name
      @boards_by_name ||= begin
        bbn = {}
        architectures.each do |architecture|
          architecture.boards_by_name.each do |board_name, board|
            bbn[board_name] = board
          end
        end
        bbn
      end
    end

    def build_settings
      {
        "runtime.hardware.path" => hardware_path.to_s,
        "runtime.ide.path" => root_path.to_s,
        "runtime.ide.version" => version,
        "runtime.os" => @os_platform
      }
    end

    include LibrariesContainer

    def lc_arduino
      self
    end

    def lc_platform
      nil
    end


  end

end

