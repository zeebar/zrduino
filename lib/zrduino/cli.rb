
require 'thor'

module Zrduino

  class CLI < Thor


    desc "version", "show version of zrduino"

    def version()
      puts Zrduino::VERSION
    end


    desc "config", "get or set the zrduino configuration"

    def config(name = nil, value = nil)
      zrd = Zrduino::Installation.new()
      if name.nil?
        puts zrd.config.config_hash.to_yaml
      elsif value.nil?
        puts zrd.config[name]
      else
        parsed_value = if ((value.to_i.to_s == value) rescue false)
          value.to_i
        elsif ((value.to_f.to_s == value) rescue false)
          value.to_f
        else
          value
        end
        zrd.config[name] = value
        Zrduino::Config.save_config(zrd.config.to_hash)
      end
    end

    desc "boards", "list board information"

    option :verbose, type: :boolean

    def boards
      zrd = Zrduino::Installation.new()
      zrd.boards.each do |board|
        puts "board '#{board.name}':"
        puts "  architecture: '#{board.arch.name}':"
        puts "  vendor: '#{board.vendor.name}':"
      end
    end

  end


end

