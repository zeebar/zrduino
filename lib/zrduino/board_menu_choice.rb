
module Zrduino 

  class BoardMenuChoice

    attr_reader :name, :menu, :title
    
    def initialize( menu, hw_conf )
      @menu = menu
      @hw_conf = hw_conf
      @name = hw_conf.name.to_sym
      @title = hw_conf.value || @name.to_s
    end


    def build_settings
      @build_settings ||= begin
        bs = {}
        @hw_conf.each_descendent do |descendent|
          unless descendent == @hw_conf
            if value = descendent.value
              bs[descendent.path(@hw_conf)] = value
            end
          end
        end
        bs 
      end
    end

  end

end

