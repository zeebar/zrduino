
module Zrduino 

  class Tool

    attr_reader :name, :platform
    
    def initialize( platform, name, hw_conf )
      @name = name
      @platform = platform
      @hw_conf = hw_conf
    end


    def arduino
      @platform.arduino
    end


    def cmd_path
      @cmd_path ||= @hw_conf['cmd.path']
    end


    def config_path
      @config_path ||= @hw_conf['config.path']
    end


    def build_settings
      @build_settings ||= begin
        { 'cmd.path' => cmd_path, 'config.path' => config_path }
      end
    end


    include RecipeContainer

    def recipes_config_root
      @hw_conf
    end

  end

end

