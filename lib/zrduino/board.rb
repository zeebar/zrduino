
module Zrduino 

  class Board

    attr_reader :name, :platform, :title
    
    def initialize( platform, name, hw_conf )
      @name = name
      @platform = platform
      @hw_conf = hw_conf
      @title = hw_conf['name']
    end

    def arduino
      @platform.arduino
    end

    def build_config
      @build_config ||= @hw_conf.children_by_name['build'] || (raise "no build config for board #{name}, only see #{@hw_conf.children.map { |child| child.name }.join(", ")}")
    end

    def core
      @core ||= begin
        core_name = build_config['core'].split(":")
        core_platform = if core_name.size == 1
          self.platform
        else
          @platform.architecture.platforms_by_vendor( core_name[0] ) || (raise "no such core platform vendor: #{core_name[0]} for board #{name}")
        end
        core_platform.cores_by_name[ core_name.last.to_sym ] || (raise "no such core: #{core_name} for board #{name}")
      end
    end

    def variant
      @variant ||= begin
        variant_name = build_config['variant'].split(":")
        variant_platform = if variant_name.size == 1
          self.platform
        else
          @platform.architecture.platforms_by_vendor( variant_name[0] ) || (raise "no such variant platform vendor: #{core_name[0]} for board #{name}")
        end
        variant_platform.variants_by_name[ variant_name.last.to_sym ] || (raise "no such variant: #{variant_name} for board #{name}")
      end
    end


    def tool_settings( tool_name )
      tool_config = @hw_conf.children_by_name[tool_name.to_s] || (raise "no tool config for board #{name} tool #{tool_name}")
      Hash[tool_config.children.map { |tc_child| ["#{tool_name}.#{tc_child.name}", tc_child.value] }]
    end


    def build_settings
      @build_settings ||= begin
        map = {}
        build_config.children.each do |child_config|
          if value = child_config.value
            map['build.' + child_config.name] = value if value
          end
        end
        map['build.core'] = core.name.to_s
        map['build.core.path'] = core.path.to_s
        map['build.variant'] = variant.name.to_s
        map['build.variant.path'] = variant.path.to_s
        map
      end
    end


    def menus_by_name
      @menus_by_name ||= begin
        cmbn = {}
        if menus_config = @hw_conf.children_by_name['menu'] 
          menus_config.children_by_name.each do |menu_name, menu_config|
            cmbn[menu_name.to_sym] = BoardMenu.new( self, menu_config )
          end
        end
        cmbn 
      end
    end

    def menus
      custom_menus_by_name.values
    end

  end

end

