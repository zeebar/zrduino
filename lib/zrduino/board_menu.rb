
module Zrduino 

  class BoardMenu

    attr_reader :name, :board
    
    def initialize( board, hw_conf )
      @board = board
      @hw_conf = hw_conf
      @name = hw_conf.name.to_sym
    end


    def choices_by_name
      @choices_by_name ||= begin
        cbn = {}
        @hw_conf.children_by_name.each do |choice_name, choice_config|
          cbn[choice_name.to_sym] = BoardMenuChoice.new(self, choice_config)
        end
        cbn 
      end
    end

    def choices
      choices_by_name.values
    end

  end

end

