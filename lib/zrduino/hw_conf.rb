
module Zrduino

  class HwConf

    attr_accessor :root, :path
 
    def [](name)
      @root.get(name)
    end

    def children
      @root.children
    end

    def parse_hw_spec_value( s )
      s
    end

    def initialize( path, os = nil ) 
      @path = path
      @root = HwConfNode.new(os)
      @path.open do |io|
        line_no = 1
        while line = io.gets
          line = line.strip
          unless line.index('#') == 0 || line.empty?
            eq_pos = line.index( '=' )
            raise "#{path.to_s}(#{line_no}): expected 'name=value', found: #{line}" unless eq_pos && eq_pos > 0
            name = line[0..eq_pos - 1].strip
            value = line[eq_pos + 1..-1]
            begin 
              parsed_value = parse_hw_spec_value( value )
              @root.set(name, parsed_value)
            rescue Exception => e
              raise "#{path.to_s}(#{line_no}): could not parse spec value '#{value}': #{e.message}, #{e.backtrace.join(", ")}"
            end
          end
          line_no += 1
        end
      end
    end

  end

end

