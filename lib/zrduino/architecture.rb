
module Zrduino 

  class Architecture

    attr_reader :name, :arduino 
    
    def initialize( arduino, name )
      @arduino = arduino
      @name = name
      @platforms_by_vendor_name = nil 
    end

    def platforms_by_vendor_name
      @platforms_by_vendor_name ||= begin
        pbv = {}
        @arduino.hardware_vendor_paths.each do |vendor_path|
          platform_path = vendor_path + name.to_s
          if platform_path.exist?
            vendor_platform = Platform.new( self, vendor_path.basename.to_s.to_sym, platform_path )
            pbv[vendor_platform.vendor_name] = vendor_platform
          end
        end
        pbv
      end
    end

    def platforms
      platforms_by_vendor_name.values
    end

    def boards_by_name
      @boards_by_name ||= begin
        bbn = {}
        platforms_by_vendor_name.each do |vendor_name, platform| 
          platform.boards.each do |board| 
            bbn[board.name] = board
          end
        end
        bbn
      end
    end

    def boards
      boards_by_name.values
    end

  end

end

