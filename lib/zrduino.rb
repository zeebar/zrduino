
require "zrduino/version"
require "zrduino/arduino"
require "zrduino/architecture"
require "zrduino/platform"
require "zrduino/tool"
require "zrduino/recipe"
require "zrduino/core"
require "zrduino/variant"
require "zrduino/hw_conf"
require "zrduino/hw_conf_node"
require "zrduino/board"
require "zrduino/board_menu"
require "zrduino/board_menu_choice"
require "zrduino/library"
require "zrduino/shell"
require "zrduino/sketch"
require "zrduino/cli"
require "zrduino/config"
require "zrduino/installation"

module Zrduino

  def installation(*args)
    Installation.new(*args)
  end

  module_function :installation

end

