# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'zrduino/version'

Gem::Specification.new do |spec|

  spec.name          = "zrduino"
  spec.version       = Zrduino::VERSION
  spec.authors       = ["Helmut Hissen"]
  spec.email         = ["helmut@zeebar.com"]

  spec.summary       = %q{Ruby and command line based development environment for Arduino.}
  spec.description   = %q{Serves as a Ruby/cli centric alternative to the Arduino GUI-only IDE while using the Arduino IDE's tool chain and hardware target configuration.}
  spec.homepage      = "http://www.zeebar.com/gems/zrduino"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "bin"
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }.select { |f| !%w{console setup}.include?(File.basename(f)) }
  spec.require_paths = ["lib"]

  spec.add_dependency "thor", "~> 0.19"

#TODO: configure
# if spec.respond_to?(:metadata)
#   spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com' to prevent pushes to rubygems.org, or delete to allow pushes to any server."
# end

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "guard", "~> 2.12"
  spec.add_development_dependency "guard-rspec", "~> 4.5"
  spec.add_development_dependency "hashdiff", "~> 0.2"

end
